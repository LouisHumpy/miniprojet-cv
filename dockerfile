FROM debian:stable-slim
MAINTAINER Jimmy Warnault

ENV DEBIAN_FRONTEND noninteractive

# setup workdir
RUN mkdir -p /root/work/
WORKDIR /root/work/

# install update
RUN apt-get -y update

# install git
RUN apt-get -y install git 
RUN apt-get clean

# install server apache
RUN apt-get -y apache2


# copy project to apache
COPY projet/* /var/www/html/

EXPOSE 80






